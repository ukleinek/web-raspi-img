---
layout: page
title: Daily auto-built images
date: 2022-02-08 11:18:03 -0600
permalink: /daily-images/
regenerate: true
---

The following images are *autobuilt daily* using the Debian Buster
(10) and Bullseye (11) repositories. They have *not been tested at
all*, and be aware that might break every now and then! ([please let
us know](mailto:gwolf@debian.org) if you think that is the
case). Available files:

## Most useful files

The following are the files you definitively want to get. The image
file is the data itself, and the shasum is useful to verify against
data corruption (most often due to interrupted downloads). Please
refer to the [instructions to flash an image]({{ 'how-to-image' | relative_url }})
for further instructions.

{% daily_table %}

## Useful for debugging

In case you have to report something, please include the relevant
files matching the version you are reporting:

### Debian 11 Stable (Bullseye)

{% debug_table bullseye %}

### Debian 12 Testing (Bookworm)

{% debug_table bookworm %}
