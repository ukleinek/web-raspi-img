---
layout: page
title: Help! Contact!
date: 2021-08-11 10:27:49 -0500
permalink: /contact/
---

Do you need help contacting us? Please read on and choose your
favorite means of contact!

I am **Gunnar Wolf `<gwolf@debian.org>`**, the maintainer for this
service. I claim any errors and inaccuracies in the built images or
the little documentation there is in https://raspi.debian.net/ . Of
course, you can approach me with any issues, although am often quite
time-stressed and may delay an answer.

The images' build instructions and infrastructure are collectively
maintained by the [Debian Raspberry Pi Maintainers
team](https://salsa.debian.org/groups/raspi-team/-/group_members).

Please keep in mind that our aim at creating these images is to give
you a system *as close as possible* to a regular Debian install. We
are probably not the best venue for general user support, choosing a
desktop environment, discussing specific daemons and so on. For user
support, please turn to the [debian-user@lists.debian.org mailing
list](mailto:debian-user@lists.debian.org) ([subscription and
information page](https://lists.debian.org/debian-user/)).

For hardware support questions, probably the best contact is the
[debian-arm@lists.debian.org mailing
list](mailto:debian-arm@lists.debian.org) ([subscription and
information page](https://lists.debian.org/debian-arm/).

You can also find us for more a informal and relaxed environment on
IRC (in the [OFTC network](https://webchat.oftc.net)), in the
following channels:

- [`#debian`](https://webchat.oftc.net/?channels=debian) for the
  general user support channel
- [`#debian-raspberrypi`](https://webchat.oftc.net/?channels=debian-raspberrypi)
  for any questions directly regarding the Raspberry images we
  distribute on this site
- [`#debian-arm`](https://webchat.oftc.net/?channels=debian-arm) for
  general ARM-related issues
